import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from '@components/login-form/login-form.component';

const loginRouter: Routes = [
  {
    path: 'login',
    component: LoginFormComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(loginRouter)],
  exports: [RouterModule],
})
export class UnauthRouterModule {}
