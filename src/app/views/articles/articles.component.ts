import { Component, OnInit } from '@angular/core';
import { ARTICLE_FIELDS } from '@constants/index';
import { Article } from '@interfaces/article';
import { ArticleService } from '@services/article/article.service';
import { Search } from 'app/shared/models/search.model';
import { tap } from 'rxjs';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css'],
})
export class ArticlesComponent implements OnInit {
  articles: Article[] = [];

  articleFields = ARTICLE_FIELDS;

  isArticleModalShown = false;
  isSearch = false;
  showNoResultsFound = false;
  isLoading = false;
  acticleFormData: any;

  public searchForm = {
    searchField: this.articleFields[0].searchKey,
    searchText: '',
  };

  private searchDynamic: Search = {
    searchName: 'searchDynamic',
    keyword: '',
  };

  // Dependency Injection
  constructor(private _articleService: ArticleService) {}

  ngOnInit(): void {
    this.fetchAllArticles();
  }

  fetchAllArticles() {
    this.isLoading = true;

    this._articleService
      .list()
      .pipe(tap(() => (this.isLoading = false)))
      .subscribe((response) => (this.articles = response));
  }

  onKeyup(event) {
    // Object Destructuring
    const { value } = event.target;
    console.log(event);
  }

  showArticleForm(formData: any) {
    this.isArticleModalShown = true;
    this.acticleFormData = formData;
  }

  hideModal() {
    this.isArticleModalShown = false;
  }

  findByType(event) {
    this.searchForm = event.value;

    if (this.searchForm.searchText !== '') {
      // Object Destructuring
      const { searchField, searchText } = this.searchForm;

      this.getSearchArticle({
        // If your key name is the same with the variable
        // ommit the value
        searchField,
        searchText,
      });

      this.isSearch = true;
    }
  }

  // SOLID
  // Single Responsibility

  setArticle(response: Article[]): void {
    this.articles = response ? [].concat(response) : [];
  }

  getSearchArticle(event: any) {
    this.searchDynamic.searchName = event.searchField;
    this.searchDynamic.keyword = event.searchText;

    this._articleService
      .searchDynamicArticle(this.searchDynamic)
      .subscribe((response) => {
        this.isSearch = true;
        this.setArticle(response);
      });
  }

  onSubmit(event) {
    this._articleService
      .save(event)
      .pipe(tap(() => (this.isLoading = false)))
      .subscribe((response) => console.log(response));
  }
}
