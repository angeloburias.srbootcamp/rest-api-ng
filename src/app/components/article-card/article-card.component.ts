import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Article } from '@interfaces/index';
import { ArticleService } from '@services/article/article.service';
import { tap } from 'rxjs';

@Component({
  selector: 'app-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.css'],
})
export class ArticleCardComponent implements OnInit {
  @Input() articleList: Article[] = [];
  @Input() isArticleModalShown = false;
  @Input() isLoading = false;

  @Output() articleEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() articleCreate: EventEmitter<any> = new EventEmitter<any>();

  articleFormData: any;

  isSubmitted = false;

  // Dependency Injection
  constructor(private _articleService: ArticleService) {}

  ngOnInit(): void {}

  emitEditArticle(event) {
    console.log(event);
    this.isArticleModalShown = true;
  }

  emitCreateArticle() {
    this.articleCreate.emit();
  }

  showArticleModal(formData: any) {
    this.articleFormData = formData;
    this.isArticleModalShown = true;
  }

  hideModal() {
    this.isArticleModalShown = false;
  }

  onSubmit(event) {
    console.log(event);
    const { id } = event;

    if (this.articleFormData) return;

    this._articleService
      .update(id, event)
      .pipe(tap(() => (this.isLoading = false)))
      .subscribe((response) => console.log(response));
  }
}
