import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ARTICLE_FIELDS } from '@constants/index';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css'],
})
export class SearchFormComponent implements OnInit {
  // Texts (Strings)
  @Input() searchFields: any[];
  @Input() searchFormvalue: any;

  // Events
  @Output() submitSearch: EventEmitter<any> = new EventEmitter<any>();
  @Output() searchFieldChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() searchTextChange: EventEmitter<any> = new EventEmitter<any>();

  searchForm: FormGroup;

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {
    this._buildSearchForm();

    if (this.searchFormvalue) this.searchForm.setValue(this.searchFormvalue);

    this.f['searchField'].valueChanges.subscribe((value) =>
      this.emitSearchFieldChange(value)
    );
  }

  private _buildSearchForm(): void {
    this.searchForm = this._fb.group({
      searchField: [''],
      searchText: [''],
    });
  }

  get f() {
    return this.searchForm.controls;
  }

  // Event Bubbling
  emitSubmitSearch(): void {
    this.submitSearch.emit(this.searchForm);
  }

  emitSearchFieldChange(event): void {
    this.searchFieldChange.emit(event);
  }

  emitSearchTextChange(event): void {
    this.searchTextChange.emit(event);
  }
}
