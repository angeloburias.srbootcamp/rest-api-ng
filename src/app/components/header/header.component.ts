import { Component, OnInit } from '@angular/core';
import { AuthService } from '@services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  user: any = '';

  constructor(private _authService: AuthService) {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  ngOnInit(): void {}

  logOut(): void {
    this._authService.logout();
  }
}
