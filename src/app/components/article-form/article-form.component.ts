import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css'],
})
export class ArticleFormComponent implements OnInit {
  @Input() formData: any;
  @Input() isOpen = false;

  @Output() hideModal: EventEmitter<any> = new EventEmitter<any>();
  @Output() submitArticle: EventEmitter<any> = new EventEmitter<any>();

  articleForm: FormGroup;

  submitted = false;

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {
    this._buildArticleForm();

    if (!this.formData) return;

    this.articleForm.patchValue(this.formData);
  }

  private _buildArticleForm() {
    this.articleForm = this._fb.group({
      title: ['', Validators.required],
      body: ['', Validators.required],
    });
  }

  get f() {
    return this.articleForm.controls;
  }

  emitHideModal(): void {
    this.hideModal.emit();
  }

  emitSubmitArticle(): void {
    this.submitted = true;
    const formValue = this.articleForm.value;

    if (this.articleForm.invalid) return;

    this.submitArticle.emit(formValue);
    this.emitHideModal();
  }
}
