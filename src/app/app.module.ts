import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticleCardComponent } from './components/article-card/article-card.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { UnauthRouterModule } from './router/unauth-router/unauth-router.module';
import { ArticlesComponent } from './views/articles/articles.component';
import { MainComponent } from './views/main/main.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { ArticleFormComponent } from './components/article-form/article-form.component';

@NgModule({
  // Components , Pipes, Directives
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    ArticlesComponent,
    LoginFormComponent,
    ArticleCardComponent,
    SearchFormComponent,
    ArticleFormComponent,
  ],
  // Modules
  imports: [
    BrowserModule,
    AppRoutingModule,
    UnauthRouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
