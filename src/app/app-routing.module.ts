import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { ArticlesComponent } from './views/articles/articles.component';
import { MainComponent } from './views/main/main.component';

const routes: Routes = [
  {
    path: 'main',
    canActivate: [AuthGuard],
    component: MainComponent,
    children: [
      {
        path: '',
        redirectTo: 'articles',
        pathMatch: 'full',
      },
      {
        path: 'articles',
        canActivate: [AuthGuard],
        component: ArticlesComponent,
      },
    ],
  },
  // Wildcard
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full',
  },
];

// Root Routes
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
