import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Article } from '@interfaces/article';
import { Search } from 'app/shared/models/search.model';
import { environment } from 'environments/environment';
import { catchError, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  apiUrl = environment.apiUrl;

  constructor(private _http: HttpClient) {}

  list(): Observable<Article[]> {
    return this._http
      .get<Article[]>(`${this.apiUrl}/posts?_limit=10`)
      .pipe(catchError(() => of([])));
  }

  save(data: Article): Observable<Article[]> {
    return this._http
      .post<Article[]>(`${this.apiUrl}/posts`, data)
      .pipe(catchError(() => of([])));
  }

  update(articleId: number, data: Article): Observable<Article[]> {
    return this._http
      .put<Article[]>(`${this.apiUrl}/${articleId}`, data)
      .pipe(catchError(() => of([])));
  }

  delete(articleId: number) {
    return this._http
      .delete<Article[]>(`${this.apiUrl}/posts/${articleId}`)
      .pipe(catchError(() => of([])));
  }

  searchDynamicArticle(search: Search): Observable<Article[]> {
    return this._http
      .get<Article[]>(
        `${this.apiUrl}/posts?${search.searchName}=${search.keyword}&_limit=10`
      )
      .pipe(catchError(() => of([])));
  }
}
