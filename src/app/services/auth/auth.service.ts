import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  token = environment.token;

  constructor(private _router: Router) {}

  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

  isLoggedIn(): boolean {
    return this.getToken() !== null;
  }

  authenticate({ email, password }: any): Observable<any> {
    if (email === 'email@example.com' && password === 'admin123') {
      this.setToken(this.token);
      localStorage.setItem(
        'user',
        JSON.stringify({ name: 'John Doe', email: 'email@example.com' })
      );
      return of({ name: 'John Doe', email: 'email@example.com' });
    }

    return throwError(() => {
      new Error('Email or Password is incorrect. Please try again!');
    });
  }

  logout(): void {
    localStorage.removeItem('token');
    this._router.navigate(['login']);
  }
}
